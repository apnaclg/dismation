let checkbox = document.getElementById('checkbox');
let btn = document.getElementById('btn');

checkbox.addEventListener('change', () => {
    if (checkbox.checked) {
        btn.style.display = 'block';
        btn.classList.add('animated-display');
    } else {
        btn.classList.remove('animated-display');
        setTimeout(function () {
            btn.style.display = 'none';
        }, 400);
    }
 }
)